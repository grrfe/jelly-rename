def group_or_none(match, idx):
    try:
        return match.group(idx)
    except IndexError:
        return None


def pad_left(_str, digits=2, pad_with="0"):
    if len(_str) < digits:
        for i in range(0, digits - len(_str)):
            _str = pad_with + _str

    return _str
