from abc import ABC, abstractmethod
from typing import Pattern, List


class EpisodePattern(ABC):
    JELLY_NAME = "Episode S{0}E{1}{2}"
    JELLY_NAME_2 = "Episode S{0}E{1}-E{2}{3}"
    JELLY_NAME_3 = "Episode S{0}E{1}{2}{3}"

    def __init__(self, pattern_num, regex: Pattern, examples: List[str]):
        self.pattern_num = pattern_num
        self.regex = regex
        self.examples = examples

    @abstractmethod
    def rename(self, match, ext) -> str:
        pass
