import re

from core.pattern.EpisodePattern import EpisodePattern
from core.util import pad_left, group_or_none


class EpisodePattern5(EpisodePattern):
    def __init__(self):
        super(EpisodePattern5, self).__init__(5, re.compile(r".*(\d{3}).*", re.IGNORECASE),
                                              ["tvs-monk-dl-ded-7p-ithd-211.mkv",
                                               "tvs-monk-dd20-ded-dl-7p-ithd-avc-601-rp.mkv",
                                               "tvs-mo-dl-ded-103-7p-iTunesHD.mkv"])

    def rename(self, match, ext: str) -> str:
        season_episode = group_or_none(match, 1)
        season = season_episode[:-2]
        episode = season_episode[-2:]

        return self.JELLY_NAME.format(pad_left(season), pad_left(episode), ext)
