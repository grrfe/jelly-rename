import re

from core.pattern.EpisodePattern import EpisodePattern
from core.util import pad_left, group_or_none


class EpisodePattern1(EpisodePattern):
    def __init__(self):
        super(EpisodePattern1, self).__init__(1, re.compile(".*S(\d+)E(\d+)([a-z]*)(?:-E(\d+))*.*", re.IGNORECASE), [
            "tvarchiv.spongebob.schwammkopf.s02e01a.mkv", "testS02E02-E03.mkv", "testS02E02.mkv",
            "Desperate.Housewives.S03E02.Das.perfekte.Paar.German.DD51.Dubbed.DL.720p.WEB-DL.x264-Mooi1990.mkv"
        ])

    def rename(self, match, ext: str) -> str:
        episode_from_till_last_match = group_or_none(match, 4)
        if episode_from_till_last_match is not None:
            return self.JELLY_NAME_2.format(pad_left(match.group(1)), pad_left(match.group(2)),
                                            episode_from_till_last_match, ext)
        else:
            episode_abc = group_or_none(match, 3)
            if episode_abc is not None:
                return self.JELLY_NAME_3.format(pad_left(match.group(1)), pad_left(match.group(2)),
                                                match.group(3), ext)
            else:
                return self.JELLY_NAME.format(pad_left(match.group(1)), pad_left(match.group(2)), ext)
