import re

from core.pattern.EpisodePattern import EpisodePattern
from core.util import pad_left


class EpisodePattern3(EpisodePattern):
    def __init__(self):
        super(EpisodePattern3, self).__init__(3, re.compile(r".*season(\d*).e(\d*).*", re.IGNORECASE),
                                              ["stars-attack.on.titan.season3.e22.1080p.mkv"])

    def rename(self, match, ext: str) -> str:
        return self.JELLY_NAME.format(pad_left(match.group(1)), pad_left(match.group(2)), ext)
