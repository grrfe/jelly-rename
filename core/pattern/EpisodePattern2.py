import re

from core.pattern.EpisodePattern import EpisodePattern
from core.util import pad_left


class EpisodePattern2(EpisodePattern):
    def __init__(self):
        super(EpisodePattern2, self).__init__(2, re.compile(r".*S(\d*)\+-\+(\d*).*", re.IGNORECASE),
                                              ["[Judas]+Dr.+Stone+S1+-+24.mkv"])

    def rename(self, match, ext: str) -> str:
        return self.JELLY_NAME.format(pad_left(match.group(1)), pad_left(match.group(2)), ext)
