import re

from core.pattern.EpisodePattern import EpisodePattern
from core.util import pad_left


class EpisodePattern4(EpisodePattern):
    def __init__(self):
        super(EpisodePattern4, self).__init__(4, re.compile(r".*\.e(\d*)\..*", re.IGNORECASE), [
            "stars-attack.on.titan.e25.1080p.mkv"
        ])

    def rename(self, match, ext: str) -> str:
        return self.JELLY_NAME.format(pad_left("1"), pad_left(match.group(1)), ext)
