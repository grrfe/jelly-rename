#!/usr/bin/python3
import argparse
import os

from pattern import EpisodePattern
from pattern.EpisodePattern1 import EpisodePattern1
from pattern.EpisodePattern2 import EpisodePattern2
from pattern.EpisodePattern3 import EpisodePattern3
from pattern.EpisodePattern4 import EpisodePattern4
from pattern.EpisodePattern5 import EpisodePattern5

__PATTERNS = [EpisodePattern1(), EpisodePattern2(), EpisodePattern3(), EpisodePattern4(), EpisodePattern5()]


def get(_dict, key, default):
    return _dict.get(key, default)


def parse_input():
    parser = argparse.ArgumentParser(description="Rename media files to properly work with Jellyfin")
    subparsers = parser.add_subparsers(help="Rename or pattern mode")

    parser_rename = subparsers.add_parser("rename", help="Rename files")
    parser_rename.add_argument("files", nargs="+", help="Files to rename")
    parser_rename.add_argument("-p", "--parser", help="Manually override parser to use")
    parser_rename.add_argument("-s", "--safe-mode", help="Print file renames instead of renaming", action="store_true")
    parser_rename.set_defaults(rename=True)

    parser_list = subparsers.add_parser("patterns", help="List patterns")
    parser_list.set_defaults(list=True)

    args = vars(parser.parse_args())

    if get(args, "rename", False):
        directory = os.getcwd()

        for f in get(args, "files", []):
            full_path = os.path.join(directory, f)
            name, ext = os.path.splitext(full_path)

            parser = get(args, "parser", default=False)
            safe_mode = get(args, "safe_mode", default=False)
            if parser is not None:
                pattern = find_parser(parser)
                res = rename(name, safe_mode, pattern, ext, full_path)
                if res is None:
                    print("Pattern did not match!")
            else:
                for pattern in __PATTERNS:
                    res = rename(name, safe_mode, pattern, ext, full_path)
                    if res is not None:
                        break

    elif get(args, "list", []):
        for pattern in __PATTERNS:
            print(f"{pattern.pattern_num}: {pattern.examples}")


def rename(name: str, safe_mode: bool, pattern: EpisodePattern, ext: str, full_path: str):
    match = pattern.regex.match(name)
    if match is not None:
        file_name = pattern.rename(match, ext)
        if safe_mode:
            print(f"{name} -> {file_name}")
        else:
            parent_directory = os.path.abspath(os.path.join(full_path, os.pardir))
            os.rename(full_path, os.path.join(parent_directory, file_name))

    return match


def find_parser(pattern_num: int):
    for pattern in __PATTERNS:
        if pattern.pattern_num == pattern_num:
            return pattern


if __name__ == "__main__":
    parse_input()
