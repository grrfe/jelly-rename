# jelly-rename

Renames files like `Rick.and.Morty.S05E02.Mortyplicity.1080p.AMZN.WEB-DL.DDP5.1.H.264-NTb[eztv.re].mkv` into the Jellyfin-friendly format `Episode S05E02.mkv`

```
grrfe@gitlab:~$ jelly-rename --help
usage: jelly-rename [-h] {rename,patterns} ...

Rename media files to properly work with Jellyfin

positional arguments:
  {rename,patterns}  Rename or pattern mode
    rename           Rename files
    patterns         List patterns

optional arguments:
  -h, --help         show this help message and exit
```

```
grrfe@gitlab:~$ jelly-rename rename --help
usage: jelly-rename rename [-h] [-p PARSER] [-s] files [files ...]

positional arguments:
  files                 Files to rename

optional arguments:
  -h, --help            show this help message and exit
  -p PARSER, --parser PARSER
                        Manually override parser to use
  -s, --safe-mode       Print file renames instead of renaming

```

Supported patterns:
```
grrfe@gitlab:~$ jelly-rename patterns
1: ['tvarchiv.spongebob.schwammkopf.s02e01a.mkv', 'testS02E02-E03.mkv', 'testS02E02.mkv', 'Desperate.Housewives.S03E02.Das.perfekte.Paar.German.DD51.Dubbed.DL.720p.WEB-DL.x264-Mooi1990.mkv']
2: ['[Judas]+Dr.+Stone+S1+-+24.mkv']
3: ['stars-attack.on.titan.season3.e22.1080p.mkv']
4: ['stars-attack.on.titan.e25.1080p.mkv']
5: ['tvs-monk-dl-ded-7p-ithd-211.mkv', 'tvs-monk-dd20-ded-dl-7p-ithd-avc-601-rp.mkv', 'tvs-mo-dl-ded-103-7p-iTunesHD.mkv']
```
